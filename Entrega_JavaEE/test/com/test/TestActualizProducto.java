package com.test;

import com.empresa.javaee.Daomodel.DaoProducto;
import com.empresa.javaee.Daomodelimpl.DaoProductoImplement;
import com.empresa.javaee.dto.Producto;

public class TestActualizProducto {

    public static void main(String[] args) {
        DaoProducto dao = new DaoProductoImplement();
        Producto p = new Producto();
        String msg;
        p.setProduct_id(2);
        p.setCode("PROD02");
        p.setName("JAVA EE ejercicios");
        p.setDescription("Libro javaEE");
        p.setPrice(10.25);

        msg = dao.upProducto(p);
        if (msg == null) {

            System.out.println("El elemento se actualizo");
        } else {
            System.out.println("No se pudo realizar su solicitud");
        }

    }

}
