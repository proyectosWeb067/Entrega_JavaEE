 
package com.empresa.javaee.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

 //aplicando singlenton a esta clase conexion
public class Conexion {
    
    public static Conexion conexion;
    private Connection conect;
    
    private Conexion(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conect = DriverManager.getConnection("jdbc:mysql://localhost:3306/productjavaee", "root", "");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public synchronized static Conexion estadoConexion(){
        if(conexion == null){
           conexion = new Conexion(); 
        } 
        return conexion;
    }
    
    public Connection getConecion(){
        return conect;
    }
    
    public void cerrarConexion(){
        conexion = null;
    }
}
