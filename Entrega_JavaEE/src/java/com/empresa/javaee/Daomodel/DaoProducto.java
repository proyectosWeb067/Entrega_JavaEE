 
package com.empresa.javaee.Daomodel;
 
import com.empresa.javaee.dto.Producto;
import java.util.List;
 

public interface DaoProducto <CualquierDto> {
    
    //metodos mostrar todos y obtener por id Producto
    public List<CualquierDto> getProductos();
    public Producto getProducto(Object producto_id);
    
    //metodos insertar-actualizar-eliminar Producto
    public String insProducto(CualquierDto p);
    public String upProducto(CualquierDto p);
    public String delProducto(Object producto_id);
    
    
}
