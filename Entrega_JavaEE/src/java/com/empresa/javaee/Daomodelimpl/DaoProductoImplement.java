package com.empresa.javaee.Daomodelimpl;

import com.empresa.javaee.Daomodel.DaoProducto;
import com.empresa.javaee.conexion.Conexion;
import com.empresa.javaee.dto.Producto;
import java.sql.CallableStatement;
 
import java.sql.ResultSet;
import java.sql.SQLException;
 
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoProductoImplement implements DaoProducto<Producto> {

    private static final Conexion CONEXION = Conexion.estadoConexion();

    @Override
    public List<Producto> getProductos() {
        List<Producto> lista = null;

        try {

            CallableStatement cs = CONEXION.getConecion().prepareCall("{call sp_getProductos}");
            ResultSet rs = cs.executeQuery();

            lista = new ArrayList<>();
            while (rs.next()) {
                Producto p = new Producto();
                p.setProduct_id(rs.getInt(1));
                p.setCode(rs.getString(2));
                p.setName(rs.getString(3));
                p.setDescription(rs.getString(4));
                p.setPrice(rs.getDouble(5));
                lista.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImplement.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrarConexion();
        }

        return lista;
    }

    @Override
    public Producto getProducto(Object producto_id) {
        Producto p = null;
        try {
            CallableStatement cs = CONEXION.getConecion().prepareCall("{call sp_getProducto(?)}");
            cs.setInt(1, (int) producto_id);
            ResultSet rs = cs.executeQuery();

            while (rs.next()) {
                p = new Producto();
                p.setProduct_id(rs.getInt(1));
                p.setCode(rs.getString(2));
                p.setName(rs.getString(3));
                p.setDescription(rs.getString(4));
                p.setPrice(rs.getDouble(5));

            }

        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImplement.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrarConexion();
        }

        return p;
    }

    @Override
    public String insProducto(Producto p) {
        String msg = null;

        try {
            CallableStatement cs = CONEXION.getConecion().prepareCall("{call sp_insproducto(?,?,?,?)}");   
             
            cs.setString(1, p.getCode());
            cs.setString(2, p.getName());
            cs.setString(3, p.getDescription());
            cs.setDouble(4, p.getPrice());
            
            int registro = cs.executeUpdate();
            
            if(registro == 0){
                msg = "Problemas al registrar";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImplement.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            CONEXION.cerrarConexion();
        }

        return msg;
    }

    @Override
    public String upProducto(Producto p) {
        String msg = null;
        try {
            CallableStatement cs = CONEXION.getConecion().prepareCall("{call sp_updproducto(?,?,?,?,?)}");
            cs.setString(1, p.getCode());
            cs.setString(2, p.getName());
            cs.setString(3, p.getDescription());
            cs.setDouble(4, p.getPrice());
            cs.setInt(5, p.getProduct_id());
            
            
            int registro = cs.executeUpdate();
            if(registro == 0){
                 msg = "Problema al actualizar";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImplement.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            CONEXION.cerrarConexion();
        }
        
        return msg;
    }

    @Override
    public String delProducto(Object producto_id) {
        String msg = null;
        
        try {
            CallableStatement cs = CONEXION.getConecion().prepareCall("{call sp_delproducto(?)}");
            cs.setInt(1, (int)producto_id);
            int registro = cs.executeUpdate();
            if(registro == 0){
                msg = "Problema al eliminar";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoProductoImplement.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            CONEXION.cerrarConexion();
        }
        return msg;
    }

}
