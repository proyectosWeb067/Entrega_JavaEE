package com.empresa.javaee.servlet;

import com.empresa.javaee.Daomodel.DaoProducto;
import com.empresa.javaee.Daomodelimpl.DaoProductoImplement;
import com.empresa.javaee.dto.Producto;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletProducto", urlPatterns = {"/Producto"})
public class ServletProducto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
       
        response.setContentType("text/html;charset=UTF-8");

        // ---parametros eliminar
        String accion = request.getParameter("accion");
        String id = request.getParameter("id");
        request.getSession().setAttribute("id", id);
        String nom = request.getParameter("nombre");
        request.getSession().setAttribute("nombre", nom);

        String tar = null;
        String msg = null;

        DaoProducto daop = new DaoProductoImplement();

       /* if (accion.equals("LOGIN")) {
            String usuario = request.getParameter("usuario");
            String pass = request.getParameter("pass");

            if (usuario == null || usuario.trim().isEmpty()) {
                msg = "Ingrese un usuario";
            } else if (pass == null || pass.trim().isEmpty()) {
                msg = "Ingrese una contraseña";
            } else if (usuario.equals("admin") && pass.equals("123456")) {
                
                HttpSession session = request.getSession(false);
                session.setAttribute("usuario", "Alexander");
                tar = "view/listar.jsp";
            } else {

                msg = "El usuario o la contraseña son Errados";

            }

        } else*/if (accion.equals("OK")) {

            List<Producto> lista = daop.getProductos();

            if (lista != null) {
                request.getSession().setAttribute("lista", lista);

                tar = "view/Producto/";
          
            }

        } else if (accion.equalsIgnoreCase("INS")) {
            Producto p = new Producto();
            msg = validar(p, request);

            if (msg == null) {
                daop.insProducto(p);

            }
            if (msg == null) {
                List<Producto> lista = daop.getProductos();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/";
            }
        } else if (accion.equals("DEL") || accion.equals("DELOK")) {

            tar = "view/Producto/delproducto.jsp";

            if (tar.equals("view/Producto/delproducto.jsp") && accion.equals("DELOK")) {
                daop.delProducto(Integer.valueOf(id));
                if (msg == null) {
                    List<Producto> lista = daop.getProductos();
                    request.getSession().setAttribute("lista", lista);

                    tar = "view/Producto/";

                }

            }

        } else if (accion.equals("GET")) {
            String idp = request.getParameter("id");
            Producto p = daop.getProducto(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/updproducto.jsp";
            } else {
                msg = "Problemas en obtener datos de Profesor";
            }

        } else if (accion.equals("UPD")) {
            Producto p = new Producto();
            msg = validar(p, request);

            if (msg == null) {
                daop.upProducto(p);
            }

            if (msg == null) {
                List<Producto> lista = daop.getProductos();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/index.jsp";
            }
        } else if (accion.equals("READ")) {
            String idp = request.getParameter("id");
            Producto p = daop.getProducto(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/readproducto.jsp";
            } else {
                msg = "Problemas en obtener datos de Profesor";
            }

        }
        if (msg != null) {
            request.getSession().setAttribute("msg", msg);
            tar = "mensaje.jsp";
        }
        if (tar != null) {
            response.sendRedirect(tar);
        }

    }

    public String validar(Producto p, HttpServletRequest request) {
        String msg;

        String product_id = request.getParameter("product_id");
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String precio = request.getParameter("precio");
        String description = request.getParameter("description");

        msg = ((codigo == null) || (codigo.trim().isEmpty()))
                ? "&iexcl;Ingrese el codigo" : null;

        if (msg == null) {
            msg = ((nombre == null) || (nombre.trim().isEmpty()))
                    ? "&iexcl;Ingrese el nombre" : null;
        }

        if (msg == null) {
            msg = ((precio == null) || (precio.trim().isEmpty()))
                    ? "&iexcl;Ingrese un precio" : null;
        }

        if (msg == null) {

            msg = ((description == null) || (description.trim().isEmpty()))
                    ? "&iexcl;Ingrese una descripción" : null;
        }

        if (msg == null) {
            if (product_id == null) {
                p.setProduct_id(null);
            } else {
                p.setProduct_id(Integer.valueOf(product_id));
            }
            p.setCode(codigo);
            p.setName(nombre);
            p.setPrice(Double.parseDouble(precio));
            p.setDescription(description);

        }

        return msg;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
