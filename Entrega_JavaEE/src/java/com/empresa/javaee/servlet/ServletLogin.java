package com.empresa.javaee.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletLogin", urlPatterns = {"/Login"})
public class ServletLogin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String accion = request.getParameter("accion");
        String tar = null;
        String msg = null;

        if (accion.equals("LOGIN")) {
            String usuario = request.getParameter("usuario");
            String pass = request.getParameter("pass");
         
            if (usuario == null || usuario.trim().isEmpty()) {
                msg = "Ingrese un usuario";
            } else if (pass == null || pass.trim().isEmpty()) {
                msg = "Ingrese una contraseña";
            }else if(usuario.equals("admin") && (!pass.equals("123456"))){
                msg = "EL usuario o la contraseña es errada";
            }
            else if (usuario.equals("admin") && pass.equals("123456")) {
                
                request.getSession().setAttribute("usuario", usuario);
                  
                tar = "view/listar.jsp";
            } else {

                msg = "El usuario o la contraseña son Errados";

            }

        }
         if (msg != null) {
            request.getSession().setAttribute("msg", msg);
            tar = "mensaje.jsp";
        }
        if (tar != null) {
            response.sendRedirect(tar);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
